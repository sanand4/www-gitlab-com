---
layout: handbook-page-toc
title: "Nira"
description: "What is Nira?"
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Nira?

**"Our mission is to make collaboration safe and secure for everyone. The sheer volume of company information that's spread across countless cloud applications makes it more challenging than ever to keep information safe and secure."** -- [Nira](https://nira.com/about/)

_That's why the creators of Nira built this tool from the ground up to help businesses like ours protect company information from unauthorized access through complete visibility, management, and control._


"Nira’s real-time access control system provides complete visibility of internal and external access to company documents. Companies get a single source of truth combining metadata from multiple APIs to provide one place to manage access for every document that employees touch. Nira currently works with Google Workplace with more integrations coming in the near future." -- [Nira](https://nira.com/about/)


## Who is Nira For?

Nira is used by team members to be able to view and modify the sharing settings of documents that they own. Nira is also used by administrators of cloud applications, typically IT & Information Security teams, to review metadata and sharing settings of documents.  

Nira's largest customers have many millions of documents in their Google Workspace, and customers include companies of all sizes from hundreds to thousands of employees.


## How Secure is Nira?

"We take the security of your data seriously. Nira has also attained its International Organization for Standardization’s (ISO) 27001 certification. Nira has achieved its SOC 2 Type 2 certification and is audited annually. Security is our highest priority and is an integral part of how we operate." -- [Nira](https://nira.com/about/)


## Why Nira?

We chose Nira because of the easy to navigate interface, alerting mechanisms, flexible dashboards, and ease of use. This helps our teams respond to incidents, assist with offboarding, and help keep our Drive files secure. 
